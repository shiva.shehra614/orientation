# Summary on Git
## What is Git ?
In a very basic manner,Git is a distributed version -control systemfor tracking changes in source code during software deelopment.
## Why Git ?
Various globle companies like GitHub , GitLAb offers software development and version control using Git.
 * Effective collaboration is the key to any computer project.
 * Git makes collaboration a lot easier and that is just a specks in it was multiverse.
 * Ranging from effective collaboration to version-control,Git makes tracking changes in source code a lot easier.

 ## Basic Terminologies
 **Repository:** Often referred to as a Repo. This is essentially where the source code (files and folders) reside that we use Git to track.
	
 - **Commit:** Saved changes are called commits. 
 
 - **Branch:** Simply put, a branch in Git is a lightweight movable pointer to one of these commits. The default branch name in Git is master. 
	As we start making commits, we are given a master branch that points to the last commit made.
	
 - **Push:** Pushing is essentially syncing the local repository with remote (accessible over the internet) branches.
 
 - **Merge:** This is pretty much self-explanatory. In a very basic sense, it is integrating two branches together.
 
 - **Clone:** Again cloning is pretty much what it sounds like. It takes the entire remote (accessible over the internet) repository and/or one or more 
	branches and makes it available locally.
	
 - **Fork:** Forking allows us to duplicate an existing repo under our username.
 ## The Git Workflow: Basics

The three prime states of a local workspace include:

 - **Working directory:** Constitutes all changes we make in our local source code on the repository.
 
 - **Staging area:** All staged files are stored in this tree of the local workspace.
 
 - **Local repo:** All committed files go to this tree of our workspace.
 
 ![Workflow](/extras/Workflow.jpg)


Now, pushing the committed changes into your remote repository is essentially required. After all, this is the whole point of Git anyway.

Therefore, simply put, the **Remote Repo** is a common repository that all team members use to collaborate on their changes.

<h4> Getting started<h4>
<h5> How to install Git<h5>

For Linux, open the terminal and type
```bash
    sudo apt-get install git
```
Clone the repo

```bash
    $ git clone <link-to-repository> 
```
Create a new branch

```bash
    $ git checkout master
    $ git checkout -b <your-branch-name>
```
You modify files in your working tree.

You selectively stage just those changes you want to be part of your next commit,
which adds only those changes to the staging area.
```bash
    $ git add .         # To add untracked files ( . adds all files) 
```
You do a commit, which takes the files as they are in the staging area and stores that
snapshot permanently to your Local Git Repository.
```bash
    $ git commit -sv   # Description about the commit
```
You do a push, which takes the files as they are in the Local Git Repository and stores
that snapshot permanently to your Remote Git Repository.
```bash
    $ git push origin <branch-name>      # push changes into repository
```
<h4>Summary<h4>
![Implementation](/extras/01.png)

 